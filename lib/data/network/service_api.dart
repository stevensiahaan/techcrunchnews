import 'dart:convert';

import 'package:steven_news/data/response/news_response.dart';
import 'package:http/http.dart' as http;
import 'package:steven_news/utils/string_resources.dart';

class ServiceApi {
  final String baseUrl = StringResources.baseUrl;

  Future<NewsResponse> getListNews() async {
    var queryParameters = {
      "sources": StringResources.sources,
      "apiKey": StringResources.apiKey,
    };

    var uri = Uri.parse(baseUrl + StringResources.headlines);
    uri = uri.replace(queryParameters: queryParameters);
    final response = await http.get(uri);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return NewsResponse.fromJson(json.decode(response.body));
    } else {
      return throw Exception("Error");
    }
  }
}
