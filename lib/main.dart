import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:steven_news/bloc/news/news_list_bloc.dart';
import 'package:steven_news/data/network/service_api.dart';
import 'package:steven_news/ui/news_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NewsListBloc>(
            create: (_) => NewsListBloc(serviceApi: ServiceApi()))
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          textTheme: const TextTheme(button: TextStyle(fontSize: 45)),
        ),
        home: const NewsScreen(),
      ),
    );
  }
}
