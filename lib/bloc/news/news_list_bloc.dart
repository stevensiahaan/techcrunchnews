import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/network/service_api.dart';
import 'news_list_event.dart';
import 'news_list_state.dart';

class NewsListBloc extends Bloc<NewsListEvent, NewsListState> {
  final ServiceApi serviceApi;

  NewsListBloc({required this.serviceApi}) : super(NewsListEmpty()) {
    on<LoadNewsList>((_, emit) async {
      try {
        emit(NewsListLoading());
        final result = await serviceApi.getListNews();

        if (result.articles != null) {
          emit(NewsListSuccess(result.articles!));
        } else {
          emit(NewsListEmpty());
        }
      } catch (error) {
        emit(NewsListError(error.toString()));
      }
    });
  }
}
