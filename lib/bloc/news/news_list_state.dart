import 'package:equatable/equatable.dart';

import '../../data/response/news_response.dart';

abstract class NewsListState extends Equatable {
  const NewsListState();

  @override
  List<Object> get props => [];
}

class NewsListEmpty extends NewsListState {}

class NewsListLoading extends NewsListState {}

class NewsListError extends NewsListState {
  final String message;

  const NewsListError(this.message);

  @override
  List<Object> get props => [message];
}

class NewsListSuccess extends NewsListState {
  final List<Article> result;

  const NewsListSuccess(this.result);

  @override
  List<Object> get props => [result];
}
