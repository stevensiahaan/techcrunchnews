import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:steven_news/data/response/news_response.dart';
import 'package:steven_news/ui/detail_news_screen.dart';

class NewsItem extends StatelessWidget {
  final Article article;

  const NewsItem({Key? key, required this.article}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 16.0,
          vertical: 8.0,
        ),
        leading: article.urlToImage == null
            ? const SizedBox(
                width: 100,
                child: Center(
                  child: Icon(
                    Icons.error,
                  ),
                ),
              )
            : CachedNetworkImage(
                imageUrl: article.urlToImage!,
                width: 100,
                fit: BoxFit.fill,
                placeholder: (context, url) => const Center(
                  child: CircularProgressIndicator(),
                ),
                errorWidget: (context, url, error) => const Center(
                  child: Icon(
                    Icons.error,
                  ),
                ),
              ),
        title: Text(
          article.title ?? "",
        ),
        subtitle: Text(article.author ?? ""),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(
            builder: (BuildContext context) {
              return DetailNewsScreen(url: article.url!);
            },
          ));
        },
      ),
    );
  }
}
