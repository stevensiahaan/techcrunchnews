import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:steven_news/bloc/news/news_list_bloc.dart';
import 'package:steven_news/bloc/news/news_list_event.dart';
import 'package:steven_news/bloc/news/news_list_state.dart';
import 'package:steven_news/data/response/news_response.dart';
import 'package:steven_news/utils/connection_utils.dart';
import 'package:steven_news/widget/news_item.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key? key}) : super(key: key);

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  var isSearch = false;
  final TextEditingController _searchQueryController = TextEditingController();
  List<Article> articleList = [];
  List<Article> filteredArticleList = [];

  @override
  void initState() {
    super.initState();
    loadNewsList();
  }

//Function to Check Connection First then Load List News if success
  loadNewsList() {
    ConnectionUtils.isInternet().then((value) {
      debugPrint(value.toString());
      if (!value) {
        showNoConnectionDialog(context);
      } else {
        BlocProvider.of<NewsListBloc>(context).add(const LoadNewsList());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          appBar: AppBar(
            title:
                isSearch ? _buildSearchField() : const Text("TechCrunch News"),
            centerTitle: true,
            actions: <Widget>[
              IconButton(
                  icon: const Icon(Icons.search),
                  onPressed: () {
                    setState(() {
                      isSearch = !isSearch;
                    });
                  })
            ],
          ),
          body: isSearch && filteredArticleList.isEmpty
              ? const Center(child: Text("News not found"))
              : BlocBuilder<NewsListBloc, NewsListState>(
                  builder: (context, state) {
                    if (state is NewsListLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is NewsListSuccess) {
                      articleList = state.result;

                      return ListView.separated(
                        separatorBuilder: (context, index) =>
                            const Divider(height: 2),
                        shrinkWrap: true,
                        itemCount: isSearch
                            ? filteredArticleList.length
                            : articleList.length,
                        itemBuilder: (context, index) {
                          return NewsItem(
                              article: isSearch
                                  ? filteredArticleList[index]
                                  : articleList[index]);
                        },
                      );
                    } else if (state is NewsListError) {
                      return Center(
                        child: Text(
                          state.message,
                        ),
                      );
                    } else {
                      return const Center(
                        child: Text(
                          '',
                        ),
                      );
                    }
                  },
                )),
    );
  }

//Show Search Field if user Click Search Button
  Widget _buildSearchField() {
    return TextField(
      controller: _searchQueryController,
      autofocus: true,
      decoration: const InputDecoration(
          border: InputBorder.none,
          hintText: "Search News...",
          hintStyle: TextStyle(color: Colors.white)),
      style: const TextStyle(color: Colors.white),
      onChanged: (query) => _onChanged(query),
    );
  }

//Detect user typing
  _onChanged(String value) {
    setState(() {
      filteredArticleList = articleList
          .where((string) =>
              string.title!.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

//Dialog to show if user has no connection
  showNoConnectionDialog(BuildContext context) {
    return showDialog<bool>(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              child: const SafeArea(
                  child: SizedBox(
                width: 300,
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    "Gagal tersambung. Periksa koneksi perangkat kamu dan coba lagi untuk login",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              )));
        });
  }

//Detect back button if user in search mode
  Future<bool> _onWillPop() {
    if (isSearch) {
      setState(() {
        isSearch = false;
        _searchQueryController.clear();
      });
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }
}
